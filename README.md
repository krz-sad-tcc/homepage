# Graduation Project Homepage

This repository contains resources for the [homepage](https://linux.ime.usp.br/~renankrz/) of our graduation final project, regarding the study of chess openings.

## Students

Renan Krzesinski

Ygor Sad Machado

## Advisors

Alfredo Goldman - IME - University of São Paulo

Johanne Cohen - LRI - Paris-Saclay University
